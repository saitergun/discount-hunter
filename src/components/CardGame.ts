const CardGame = (game: IGame) => {
  return `
    <section class="card-game">
      <header class="card-game__header">
        <img class="card-game__cover" src="${ game.thumb }" />
      </header>

      <main class="card-game__main">
        <h3 class="card-game__title">${ game.external }</h3>
      </main>

      <footer class="card-game__footer">
        <div class="card-game__priceGroup">
          <h5 class="card-game__price">$${ game.cheapestDeal.price }</h5>

          <h6 class="card-game__priceOld">$${ game.cheapestDeal.retailPrice }</h6>
        </div>

        <span class="card-game__discount">
          <span>${ Number(game.cheapestDeal.savings).toFixed(0) }</span>

          <span class="card-game__discount__percent">%</span>
        </span>
      </footer>
    </section>
  `
}

export default CardGame

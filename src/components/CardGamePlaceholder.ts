const CardGamePlaceholder = () => {
  return `
    <section class="card-game-placeholder">
      <header class="card-game-placeholder__header">
        <span class="card-game-placeholder__cover" />
      </header>

      <main class="card-game-placeholder__main">
        <span class="card-game-placeholder__title" />
      </main>

      <footer class="card-game-placeholder__footer">
        <div class="card-game-placeholder__priceGroup">
          <span class="card-game-placeholder__price" />

          <span class="card-game-placeholder__priceOld" />
        </div>

        <span class="card-game-placeholder__discount" />
      </footer>
    </section>
  `
}

export default CardGamePlaceholder

import './style/index.scss'

import {
  HEADER_SEARCH_INPUT,
  handleSearch,
} from './util/util'

window.addEventListener('load', () => {
  document.body.style.display = ''

  handleSearch('batman')

  HEADER_SEARCH_INPUT.value = 'batman'
})

HEADER_SEARCH_INPUT.addEventListener('keypress', (e: KeyboardEvent) => {
  if (e.key !== 'Enter') {
    return
  }

  handleSearch(HEADER_SEARCH_INPUT.value)
})

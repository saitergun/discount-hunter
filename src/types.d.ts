interface IGame {
  gameID: number;
  steamAppID: number | null;
  cheapest: number;
  cheapestDealID: string;
  external: string;
  internalName: string;
  thumb: string;
  cheapestDeal: IGameDealDeal;
}

interface IGameDeal {
  info: {
    title: string;
    steamAppID: string | null;
    thumb: string;
  };

  cheapestPriceEver: {
    price: string;
    date: number;
  };

  deals?: IGameDealDeal[];
}

interface IGameDealDeal {
  storeID: number;
  dealID: string;
  price: number;
  retailPrice: number;
  savings: number;
}

import axios from 'axios'

const request = axios.create({
  baseURL: 'https://www.cheapshark.com/api/1.0',
})

export default request

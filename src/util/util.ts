import request from './request'

import CardGame from '../components/CardGame'
import CardGamePlaceholder from '../components/CardGamePlaceholder'

export const APP_HEADER = document.querySelector<HTMLDivElement>('#app-header')!
export const APP_NAVBAR = document.querySelector<HTMLDivElement>('#app-navbar')!
export const APP_MAIN = document.querySelector<HTMLDivElement>('#app-main')!

export const HEADER_SEARCH_INPUT = document.querySelector<HTMLInputElement>('#app-header-search-input')!

export const SEARCH_RESULT_CONTAINER = document.querySelector<HTMLDivElement>('#search-result-container')!
export const SEARCH_RESULT_TITLE = document.querySelector<HTMLHeadingElement>('#search-result-title')!
export const SEARCH_RESULT_MESSAGE = document.querySelector<HTMLParagraphElement>('#search-result-message')!
export const SEARCH_RESULT_MESSAGE_PLACEHOLDER = document.querySelector<HTMLSpanElement>('#search-result-message-placeholder')!

export const getGames = async (title: string) => {
  try {
    const response = await request.get<IGame[]>('games', {
      params: {
        title,
      }
    })

    return response.data
  } catch (error) {
    throw error
  }
}

export const getGameDeals = async (gameIds: number[] = []) => {
  try {
    const response = await request('games', {
      params: {
        ids: gameIds.toString()
      }
    })

    return response.data
  } catch (error) {
    throw error
  }
}

export const handleGames = (games: IGame[] = [], gameDeals: Record<IGame['gameID'], IGameDeal> = {}) => {
  return games
    .map((game) => {
      const deals = gameDeals[game.gameID]?.deals

      if (!deals) {
        return game
      }

      const [cheapestDeal] = deals

      return { ...game, deals, cheapestDeal }
    })
    .filter((game) => game.cheapestDeal && game.cheapestDeal.savings > 0)
    .sort((a, b) => b.cheapestDeal.savings - a.cheapestDeal.savings)
}

export const createGamePlaceholdersHtmlString = () => {
  let html = ''

  for (let i = 0; i < 12; i++) {
    html += `
      <div class="col-span-12 sm:col-span-6 lg:col-span-4 xl:col-span-3">
        ${CardGamePlaceholder()}
      </div>
    `
  }

  return html
}

export const createGamesHtmlString = (games: IGame[] = []) => {
  let html = ''

  games.forEach((game) => {
    html += `
      <div class="col-span-12 sm:col-span-6 lg:col-span-4 xl:col-span-3">
        ${CardGame(game)}
      </div>
    `
  })

  return html
}

export const createAlertHtmlString = (message: string) => {
  return `
    <div class="inline-flex items-start gap-4 bg-red-100 text-red-600 border-l-4 border-red-500 rounded-l rounded-r-xl p-4">
      <svg class="text-24/16" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="1em" height="1em">
        <path fill="currentColor" d="M12 22C6.477 22 2 17.523 2 12S6.477 2 12 2s10 4.477 10 10-4.477 10-10 10zm0-2a8 8 0 1 0 0-16 8 8 0 0 0 0 16zm-1-5h2v2h-2v-2zm0-8h2v6h-2V7z" />
      </svg>

      <p>${ message }</p>
    </div>
  `
}

export const handleSearch = async (keyword: string) => {
  SEARCH_RESULT_MESSAGE.style.display = ''

  SEARCH_RESULT_TITLE.innerText = `Search Result: ${keyword}`
  SEARCH_RESULT_CONTAINER.innerHTML = createGamePlaceholdersHtmlString()

  let games = await getGames(keyword)
  let gameDeals = await getGameDeals(games.map((game) => game.gameID))

  const filteredGames = handleGames(games, gameDeals)

  SEARCH_RESULT_MESSAGE_PLACEHOLDER.style.display = 'none'

  if (filteredGames.length > 0) {
    SEARCH_RESULT_CONTAINER.innerHTML = createGamesHtmlString(filteredGames)

    SEARCH_RESULT_MESSAGE.innerText = `${filteredGames.length} discounted games found.`
    SEARCH_RESULT_MESSAGE.style.display = 'block'
  } else {
    SEARCH_RESULT_CONTAINER.innerHTML = `<div class="col-span-12">${createAlertHtmlString('no result for your search')}</div>`
  }
}
